using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DialogueLoadData
{
    public string NodeGUID;
    public string NodeName;
    public string DialogueText;
    public Rect NodePositionAndSize;
}
