using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

public class DialogueSearchWindow : ScriptableObject, ISearchWindowProvider
{
    DialogueGraphView graphView;
    Texture2D intentionIcon;
    EditorWindow viewContainingWindow;
    public void Init(DialogueGraphView graphView, EditorWindow editorWindow)
    {
        this.viewContainingWindow = editorWindow; 
        this.graphView = graphView;

        intentionIcon = new Texture2D(1, 1);
        intentionIcon.SetPixel(0, 0, new Color(0, 0, 0, 0));
        intentionIcon.Apply();
    }
    public List<SearchTreeEntry> CreateSearchTree(SearchWindowContext context)
    {
        var entries = new List<SearchTreeEntry>();
        entries.Add(new SearchTreeGroupEntry(new GUIContent("Create Nodes"), 0));

        entries.Add(new SearchTreeGroupEntry(new GUIContent("Dialogue Nodes"), 1));
        foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
        {
            foreach (var type in assembly.GetTypes())
            {
                if (type.IsClass && !type.IsAbstract && (type.IsSubclassOf(typeof(DialogueNode))))
                {
                    string space = "   ";
                    entries.Add(new SearchTreeEntry(new GUIContent(space + type.Name, intentionIcon)) { level = 2, userData = type });
                }
            }
        }

        entries.Add(new SearchTreeGroupEntry(new GUIContent("Test Nodes"), 1));
        entries.Add(new SearchTreeEntry(new GUIContent("Test Node 1")) { level = 2, });
        entries.Add(new SearchTreeEntry(new GUIContent("Test Node 2")) { level = 2, });

        return entries;
    }

    public bool OnSelectEntry(SearchTreeEntry searchTreeEntry, SearchWindowContext context)
    {
        var type = searchTreeEntry.userData as System.Type;
        if (type == null)
        {
            EditorUtility.DisplayDialog("Create Failed", "Invalid type !!", "OK");
            return false;
        }
        var node = graphView.CreateDialogueNode(type.Name);
        var pos = graphView.GetLocalMousePosition(context.screenMousePosition);

        node.SetPosition(new Rect(pos, new Vector2(200, 150)));
        graphView.AddElement(node);
        return true;
    }
}
